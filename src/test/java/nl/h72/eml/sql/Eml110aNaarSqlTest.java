/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.TestHulpmiddel.assertDatabaseInhoud;
import static nl.h72.eml.sql.generated.Tables.PARTIJEN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.File;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.ApplicationArguments;

import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Test class voor {@link Eml110aNaarSql}.
 */
@ExtendWith(MockitoExtension.class)
class Eml110aNaarSqlTest {

  private static final String VERKIEZINGSDEFINITIE_TK2023 = "Verkiezingsdefinitie_TK2023.eml.xml";
  private static final String VERKIEZINGSDEFINITIE_PS2023 = "Verkiezingsdefinitie_PS2023_Fryslan.eml.xml";
  private static final String VERKIEZINGSDEFINITIE_EP2024 = "Verkiezingsdefinitie_EP.eml.xml";
  private static final String KANDIDATENLIJSTEN_EP2024 = "Kandidatenlijsten_EP.eml.xml";

  private @Mock ApplicationArguments applicationArguments;
  private @TempDir File tijdelijkeDirectory;

  @Test
  void testInlezenTK2023() throws Exception {
    assertDatabaseInhoud(Eml110aNaarSqlTest::assertInhoudDatabaseTK2023, tijdelijkeDirectory, VERKIEZINGSDEFINITIE_TK2023);
  }

  private static void assertInhoudDatabaseTK2023(final DatabaseContext dc) {
    TestHulpmiddel.assertTelPartijen(dc, 26);
    assertEquals("VVD", dc.getDslContext().select(PARTIJEN.NAAM).from(PARTIJEN).fetchAny().getValue(0),
        "Niet de verwachte partijnaam voor eerste partij in de lijst");
  }

  @Test
  void testInlezenPS2023() throws Exception {
    assertDatabaseInhoud(Eml110aNaarSqlTest::assertInhoudDatabasePS2023, tijdelijkeDirectory, VERKIEZINGSDEFINITIE_PS2023);
  }

  private static void assertInhoudDatabasePS2023(final DatabaseContext dc) {
    assertNull(dc.getDslContext().select(PARTIJEN.NAAM).from(PARTIJEN).fetchAny(), "Partijen worden niet ingeladen als contest id 'geen' is.");
  }

  @Test
  void testInlezenEP2024() throws Exception {
    assertDatabaseInhoud(Eml110aNaarSqlTest::assertInhoudDatabaseEP2024, tijdelijkeDirectory, KANDIDATENLIJSTEN_EP2024, VERKIEZINGSDEFINITIE_EP2024);
  }

  private static void assertInhoudDatabaseEP2024(final DatabaseContext dc) {
    assertEquals("Partij 1", dc.getDslContext().select(PARTIJEN.NAAM).from(PARTIJEN).fetchAny().getValue(0),
        "Niet de verwachte partijnaam voor eerste partij in de lijst");
  }
}
