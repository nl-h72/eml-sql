/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.PARTIJEN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Consumer;

import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.springframework.boot.ApplicationArguments;

import nl.h72.eml.config.ApplicatieConfiguratie;
import nl.h72.eml.config.DatabaseConfiguratie;
import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Hulpmiddelen om in meerdere testen te gebruiken.
 */
final class TestHulpmiddel {

  private TestHulpmiddel() {
    // Hulpmiddel class
  }

  public static void assertDatabaseInhoud(final Consumer<DatabaseContext> consument, final File tijdelijkeDirectory, final String... bestandsnamen)
      throws Exception {
    final ApplicationArguments applicationArguments = mock(ApplicationArguments.class);
    TestHulpmiddel.kopieerTestbestandNaarTijdelijkeMap(tijdelijkeDirectory, bestandsnamen);
    final EmlNaarSql emlNaarSql = TestHulpmiddel.maakEmlNaarSql(tijdelijkeDirectory);
    TestHulpmiddel.mockApplicationArguments(applicationArguments, tijdelijkeDirectory.getAbsolutePath());

    emlNaarSql.run(applicationArguments);
    TestHulpmiddel.wrapDatabaseAanroep(consument, tijdelijkeDirectory);
  }

  public static void mockApplicationArguments(final ApplicationArguments applicationArguments, final String uitslagenDirectory) {
    doReturn(null).when(applicationArguments).getOptionValues("help");
    lenient().doReturn(List.of(uitslagenDirectory)).when(applicationArguments).getNonOptionArgs();
  }

  public static String maakDatabaseUrl(final File directory) {
    return new File(directory, "eml.sqlite").getAbsolutePath();
  }

  public static OpdrachtregelLezer maakOpdrachtregelLezer(final File directory) {
    final DatabaseConfiguratie dbConfig = new DatabaseConfiguratie();
    dbConfig.setDialect("sqlite");
    dbConfig.setUrl(maakDatabaseUrl(directory));

    return new OpdrachtregelLezer(new ApplicatieConfiguratie(), dbConfig);
  }

  public static EmlNaarSql maakEmlNaarSql(final File directory) {

    final OpdrachtregelLezer opdrachtregelLezer = TestHulpmiddel.maakOpdrachtregelLezer(directory);

    return new EmlNaarSql(opdrachtregelLezer);
  }

  public static void kopieerTestbestandNaarTijdelijkeMap(final File tijdelijkeMap, final String... bestandsnamen) throws IOException, URISyntaxException {
    for (String bestandsnaam : bestandsnamen) {
       Files.copy(Path.of(TestHulpmiddel.class.getResource(bestandsnaam).toURI()), Path.of(tijdelijkeMap.getAbsolutePath(), bestandsnaam));
    }
  }

  public static void wrapDatabaseAanroep(final Consumer<DatabaseContext> consument, final File directory) throws SQLException {
    SqlHulpmiddelen.wrapDatabaseConnectie(consument, SQLDialect.SQLITE, DatabaseMaker.databaseUrl(SQLDialect.SQLITE, maakDatabaseUrl(directory)), "",
        "");
  }

  public static void assertTelPartijen(final DatabaseContext dc, final int aantalPartijen) {
    assertEquals(aantalPartijen, dc.getDslContext().select(DSL.count(PARTIJEN.NAAM)).from(PARTIJEN).fetchAny().getValue(0),
        "Aantal ingelezen partijen klopt niet.");
  }
}
