/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.TestHulpmiddel.assertDatabaseInhoud;
import static nl.h72.eml.sql.generated.Tables.KANDIDATEN;
import static nl.h72.eml.sql.generated.Tables.KIESKRINGEN;
import static nl.h72.eml.sql.generated.Tables.PARTIJEN;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.ApplicationArguments;

import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Test class voor {@link Eml230bNaarSql}.
 */
@ExtendWith(MockitoExtension.class)
class Eml230bNaarSqlTest {

  private static final String KANDIDATENLIJST_PS2023 = "Kandidatenlijsten_PS2023_Fryslan.eml.xml";

  private @Mock ApplicationArguments applicationArguments;
  private @TempDir File tijdelijkeDirectory;

  @Test
  void testInlezenTK2023() throws Exception {
    assertDatabaseInhoud(Eml230bNaarSqlTest::assertInhoudDatabasePS2023, tijdelijkeDirectory, KANDIDATENLIJST_PS2023);
  }

  private static void assertInhoudDatabasePS2023(final DatabaseContext dc) {
    assertEquals("Fryslân", dc.getDslContext().select(KIESKRINGEN.KIESKRING_NAAM).from(KIESKRINGEN).fetchAny().getValue(0),
        "Niet de verwachte kieskring naam voor eerste kieskring in de lijst");
    TestHulpmiddel.assertTelPartijen(dc, 17);
    assertEquals("CDA", dc.getDslContext().select(PARTIJEN.NAAM).from(PARTIJEN).fetchAny().getValue(0),
        "Niet de verwachte partijnaam voor eerste partij in de lijst");
    assertEquals("Douwstra", dc.getDslContext().select(KANDIDATEN.ACHTERNAAM).from(KANDIDATEN).fetchAny().getValue(0),
        "Niet de verwachte achternaam voor eerste kandidaat in de lijst");
  }
}
