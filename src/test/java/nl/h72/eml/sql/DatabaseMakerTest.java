/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.sql.SQLException;

import org.jooq.SQLDialect;
import org.junit.jupiter.api.Test;

/**
 * Test class om {@link DatabaseMaker} class te testen.
 */
class DatabaseMakerTest {

  @Test
  void testMaakSqliteDatabase() throws URISyntaxException, SQLException, IOException {
    final File databaseBestand = new File(Path.of(getClass().getClassLoader().getResource(".").toURI()).toFile(), "eml-test.sqlite");

    if (databaseBestand.exists()) {
      assertTrue(databaseBestand.delete(), "Database bestaat, maar kon niet worden verwijderd, wat wel nodig is voor deze test");
    }
    DatabaseMaker.maakDatabaseSchema(DatabaseMaker.databaseUrl(SQLDialect.SQLITE, databaseBestand.getAbsolutePath()), "", "");
    System.out.println(DatabaseMaker.databaseUrl(SQLDialect.SQLITE, databaseBestand.getAbsolutePath()));
    assertTrue(databaseBestand.exists(), "Database bestand bestaat niet terwijl dat wel zou moeten");
  }
}
