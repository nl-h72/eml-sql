--
-- Copyright 2024 EML-SQL bijdragers
--
-- In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
-- goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
--
-- U mag dit werk alleen gebruiken in overeenstemming met de licentie.
-- U kunt een kopie van de licentie verkrijgen op:
--
-- https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
--
-- Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
-- wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
-- een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
-- expliciet of impliciet.
-- Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
-- onder de Licentie van toepassing zijn.
--

CREATE TABLE kandidaten (
  verkiezing_id TEXT NOT NULL,
  kieskring_id INTEGER NOT NULL,
  partij_id INTEGER NOT NULL,
  kandidaat_id INTEGER NOT NULL,
  kandidaat_code TEXT,
  geslacht TEXT NOT NULL,
  voorletters TEXT NOT NULL,
  roepnaam TEXT NOT NULL,
  tussenvoegsel TEXT NOT NULL,
  achternaam TEXT NOT NULL,
  woonplaats TEXT,
  land_code TEXT,

  CONSTRAINT kandidaten_pkey PRIMARY KEY (verkiezing_id, kieskring_id, partij_id, kandidaat_id),
  CONSTRAINT kandidaten_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen,
  CONSTRAINT kandidaten_fkey_partijen FOREIGN KEY (verkiezing_id, kieskring_id, partij_id) REFERENCES partijen
);

CREATE TABLE kandidaten_stemmen (
  verkiezing_id TEXT NOT NULL,
  autoriteit_id TEXT NOT NULL,
  kieskring_id INTEGER NOT NULL,
  rapportage_gebied_id TEXT NOT NULL,
  partij_id INTEGER NOT NULL,
  kandidaat_id INTEGER NOT NULL,
  aantal_stemmen INTEGER NOT NULL,

  CONSTRAINT kandidaten_stemmen_pkey PRIMARY KEY (verkiezing_id, autoriteit_id, rapportage_gebied_id, partij_id, kandidaat_id),
  CONSTRAINT kandidaten_stemmen_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen,
  CONSTRAINT kandidaten_stemmen_fkey_rapportage_gebieden FOREIGN KEY (verkiezing_id, autoriteit_id, rapportage_gebied_id) REFERENCES rapportage_gebieden,
  CONSTRAINT kandidaten_stemmen_fkey_kandidaten FOREIGN KEY (verkiezing_id, kieskring_id, partij_id, kandidaat_id) REFERENCES kandidaten
);

CREATE TABLE gekozen_kandidaten (
  verkiezing_id TEXT NOT NULL,
  kieskring_id INTEGER NOT NULL,
  partij_id INTEGER NOT NULL,
  partij_naam TEXT NOT NULL,
  kandidaat_id INTEGER NOT NULL,
  kandidaat_code TEXT,
  geslacht TEXT NOT NULL,
  voorletters TEXT NOT NULL,
  roepnaam TEXT NOT NULL,
  tussenvoegsel TEXT NOT NULL,
  achternaam TEXT NOT NULL,
  woonplaats TEXT,
  land_code TEXT,
  gekozen INTEGER DEFAULT 0,
  rangschikking INTEGER DEFAULT -1,

  CONSTRAINT gekozen_kandidaten_pkey PRIMARY KEY (verkiezing_id, kieskring_id, partij_id, kandidaat_id),
  CONSTRAINT gekozen_kandidaten_stemmen_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen,
  CONSTRAINT gekozen_kandidaten_fkey_kandidaten FOREIGN KEY (verkiezing_id, kieskring_id, partij_id, kandidaat_id) REFERENCES kandidaten
)
