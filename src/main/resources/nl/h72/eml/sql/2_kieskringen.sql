--
-- Copyright 2024 EML-SQL bijdragers
--
-- In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
-- goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
--
-- U mag dit werk alleen gebruiken in overeenstemming met de licentie.
-- U kunt een kopie van de licentie verkrijgen op:
--
-- https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
--
-- Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
-- wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
-- een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
-- expliciet of impliciet.
-- Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
-- onder de Licentie van toepassing zijn.
--

CREATE TABLE kieskringen (
  verkiezing_id TEXT NOT NULL,
  kieskring_id INTEGER,
  kieskring_naam TEXT,

  CONSTRAINT kieskringen_pkey PRIMARY KEY (verkiezing_id, kieskring_id)
);

CREATE TABLE regios (
  verkiezing_id TEXT NOT NULL,
  regio_id TEXT NOT NULL,
  naam TEXT NOT NULL,
  categorie TEXT NOT NULL,
  commissie_categorie TEXT NOT NULL,
  ouder_regio_id TEXT,
  ouder_regio_categorie TEXT,
  friese_export_toegestaan INTEGER DEFAULT 0,
  romeinse_cijfers INTEGER DEFAULT 0,

  CONSTRAINT regios_pkey PRIMARY KEY (verkiezing_id, regio_id, categorie),
  CONSTRAINT regios_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen
);

CREATE TABLE rapportage_gebieden (
  verkiezing_id TEXT NOT NULL,
  autoriteit_id TEXT NOT NULL,
  rapportage_gebied_id TEXT NOT NULL,
  eml_id TEXT NOT NULL,
  naam TEXT NOT NULL,
  postcode TEXT,

  CONSTRAINT rapportage_gebieden_pkey PRIMARY KEY (verkiezing_id, autoriteit_id, rapportage_gebied_id),
  CONSTRAINT rapportage_gebieden_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen
);

CREATE TABLE rapportage_gebieden_statistieken (
  verkiezing_id TEXT NOT NULL,
  autoriteit_id TEXT NOT NULL,
  rapportage_gebied_id TEXT NOT NULL,
  naam TEXT NOT NULL,
  type TEXT NOT NULL,
  stemmen_afgewezen INTEGER NOT NULL,
  stemmen_ongeteld INTEGER NOT NULL,
  telling INTEGER NOT NULL,

  CONSTRAINT rapportage_gebieden_statistieken_pkey PRIMARY KEY (verkiezing_id, autoriteit_id, rapportage_gebied_id, type),
  CONSTRAINT rapportage_gebieden_statistieken_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen,
  CONSTRAINT rapportage_gebieden_statistieken_fkey_rapportage_gebieden FOREIGN KEY (verkiezing_id, autoriteit_id, rapportage_gebied_id) REFERENCES rapportage_gebieden
)
