--
-- Copyright 2024 EML-SQL bijdragers
--
-- In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
-- goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
--
-- U mag dit werk alleen gebruiken in overeenstemming met de licentie.
-- U kunt een kopie van de licentie verkrijgen op:
--
-- https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
--
-- Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
-- wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
-- een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
-- expliciet of impliciet.
-- Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
-- onder de Licentie van toepassing zijn.
--

CREATE TABLE partijen (
  verkiezing_id TEXT NOT NULL,
  kieskring_id INTEGER NOT NULL,
  partij_id INTEGER NOT NULL,
  naam TEXT NOT NULL,
  lijst_type TEXT NOT NULL,

  CONSTRAINT partijen_pkey PRIMARY KEY (verkiezing_id, kieskring_id, partij_id),
  CONSTRAINT partijen_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen,
  CONSTRAINT partijen_fkey_kieskringen FOREIGN KEY (verkiezing_id, kieskring_id) REFERENCES kieskringen
);

CREATE TABLE partijen_stemmen (
  verkiezing_id TEXT NOT NULL,
  kieskring_id INTEGER NOT NULL,
  autoriteit_id TEXT NOT NULL,
  rapportage_gebied_id TEXT NOT NULL,
  partij_id INTEGER NOT NULL,
  naam TEXT NOT NULL,
  aantal_stemmen INTEGER NOT NULL,

  CONSTRAINT partijen_stemmen_pkey PRIMARY KEY (verkiezing_id, autoriteit_id, rapportage_gebied_id, partij_id),
  CONSTRAINT partijen_stemmen_fkey_verkiezingen FOREIGN KEY (verkiezing_id) REFERENCES verkiezingen,
  CONSTRAINT partijen_stemmen_fkey_rapportage_gebieden FOREIGN KEY (verkiezing_id, autoriteit_id, rapportage_gebied_id) REFERENCES rapportage_gebieden,
  CONSTRAINT partijen_stemmen_fkey_partijen FOREIGN KEY (verkiezing_id, kieskring_id, partij_id) REFERENCES partijen
)
