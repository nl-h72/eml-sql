--
-- Copyright 2024 EML-SQL bijdragers
--
-- In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
-- goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
--
-- U mag dit werk alleen gebruiken in overeenstemming met de licentie.
-- U kunt een kopie van de licentie verkrijgen op:
--
-- https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
--
-- Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
-- wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
-- een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
-- expliciet of impliciet.
-- Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
-- onder de Licentie van toepassing zijn.
--

CREATE TABLE verkiezingen (
  verkiezing_id TEXT NOT NULL,
  naam TEXT NOT NULL,
  datum TIMESTAMP NOT NULL,
  categorie TEXT NOT NULL,
  aantal_zetels INTEGER NOT NULL,
  voorkeursdrempel INTEGER NOT NULL,

  CONSTRAINT verkiezing_id_pkey PRIMARY KEY (verkiezing_id)
)
