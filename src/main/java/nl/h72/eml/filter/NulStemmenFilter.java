/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.filter;

import nl.h72.eml.omzetten.EmlFilter;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.PartijStemmen;

/**
 * EmlFilter waarbij alle partijen en kandidaten die 0 stemmen hebben worden weggelaten.
 */
public class NulStemmenFilter implements EmlFilter {
  @Override
  public boolean filter(final PartijStemmen partijStemmen) {
    return partijStemmen.aantalStemmen() > 0;
  }

  @Override
  public boolean filter(final KandidaatStemmen kandidaatStemmen) {
    return kandidaatStemmen.aantalStemmen() > 0;
  }
}
