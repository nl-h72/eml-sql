/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.KANDIDATEN;
import static nl.h72.eml.sql.generated.Tables.KIESKRINGEN;
import static nl.h72.eml.sql.generated.Tables.PARTIJEN;
import static nl.h72.eml.sql.generated.Tables.VERKIEZINGEN;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Stream;

import org.jooq.DSLContext;
import org.jooq.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import nl.h72.eml.records.Eml230b;
import nl.h72.eml.records.Kandidaat;
import nl.h72.eml.records.Kieskring;
import nl.h72.eml.records.Partij;
import nl.h72.eml.records.Verkiezing;
import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Zet EML230b bestanden in de database.
 */
class Eml230bNaarSql {
  private final DSLContext dslContext;
  private final Eml230b eml230b;

  private Eml230bNaarSql(final DSLContext dslContext, final Eml230b eml230b) {
    this.dslContext = dslContext;
    this.eml230b = eml230b;
  }

  @Transactional(propagation = Propagation.MANDATORY)
  public static void voegInDatabase(final DatabaseContext databaseContext, final Eml230b eml230) {
    databaseContext.transactieUitvoeren(dslContext -> voegVerkiezingIn(dslContext, eml230));

    databaseContext.batchUitvoeren(dslContext -> {
      final Eml230bNaarSql batcher = new Eml230bNaarSql(dslContext, eml230);

      return batcher.voegKieskringenIn();
    }).join();
  }

  private static Query voegVerkiezingIn(final DSLContext dslContext, final Eml230b eml230) {
    final Verkiezing verkiezing = eml230.verkiezing();

    return dslContext
        .insertInto(VERKIEZINGEN, VERKIEZINGEN.VERKIEZING_ID, VERKIEZINGEN.NAAM, VERKIEZINGEN.DATUM, VERKIEZINGEN.CATEGORIE,
            VERKIEZINGEN.AANTAL_ZETELS, VERKIEZINGEN.VOORKEURSDREMPEL)
        .values(verkiezing.verkiezingId(), verkiezing.naam(), LocalDateTime.of(LocalDate.parse(verkiezing.datum()), LocalTime.MIDNIGHT),
            verkiezing.categorie(), 0, 0)
        .onDuplicateKeyIgnore();
  }

  private List<Query> voegKieskringenIn() {
    return eml230b.kieskringen().stream().flatMap(k -> voegKieskringIn(eml230b.verkiezing(), k)).toList();
  }

  private Stream<Query> voegKieskringIn(final Verkiezing verkiezing, final Kieskring kiesKring) {
    final Integer kieskringId = Hulpmiddel.kieskringId(kiesKring.kieskringId(), verkiezing.verkiezingsDomeinId());
    final List<Query> kieskring = List
        .of(dslContext.insertInto(KIESKRINGEN, KIESKRINGEN.VERKIEZING_ID, KIESKRINGEN.KIESKRING_ID, KIESKRINGEN.KIESKRING_NAAM)
            .values(verkiezing.verkiezingId(), kieskringId, kiesKring.naam()));

    return Stream.concat(kieskring.stream(), kiesKring.partijen().stream().flatMap(p -> voegPartijIn(verkiezing.verkiezingId(), kieskringId, p)));
  }

  private Stream<Query> voegPartijIn(final String verkiezingId, final Integer kieskringId, final Partij partij) {
    final List<Query> partijQuery = List
        .of(dslContext.insertInto(PARTIJEN, PARTIJEN.VERKIEZING_ID, PARTIJEN.KIESKRING_ID, PARTIJEN.PARTIJ_ID, PARTIJEN.NAAM, PARTIJEN.LIJST_TYPE)
            .values(verkiezingId, kieskringId, partij.partijId(), partij.naam(), partij.lijstType().getValue()));

    return Stream.concat(partijQuery.stream(),
        partij.kandidaten().stream().map(k -> voegKandidaatIn(verkiezingId, kieskringId, partij.partijId(), k)));
  }

  private Query voegKandidaatIn(final String verkiezingId, final Integer kieskringId, final int partijId, final Kandidaat kandidaat) {
    return dslContext.insertInto(KANDIDATEN, KANDIDATEN.VERKIEZING_ID, KANDIDATEN.KIESKRING_ID, KANDIDATEN.PARTIJ_ID, KANDIDATEN.KANDIDAAT_ID,
        KANDIDATEN.GESLACHT, KANDIDATEN.VOORLETTERS, KANDIDATEN.ROEPNAAM, KANDIDATEN.TUSSENVOEGSEL, KANDIDATEN.ACHTERNAAM, KANDIDATEN.WOONPLAATS,
        KANDIDATEN.LAND_CODE)
        .values(verkiezingId, kieskringId, partijId, kandidaat.kandidaatId(), kandidaat.geslacht(), kandidaat.voorletters(), kandidaat.roepnaam(),
            kandidaat.tussenvoegsel(), kandidaat.achternaam(), kandidaat.woonplaats(), kandidaat.landCode());
  }
}
