/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.tables.Verkiezingen.VERKIEZINGEN;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jooq.SQLDialect;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class om database schema; tabellen en views aan te maken. Daarnaast hulp functies voor maken database connectie.
 */
class DatabaseMaker {

  private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseMaker.class);
  private static final List<String> SQL_FILES = List.of("1_verkiezingen", "2_kieskringen", "3_partijen", "4_kandidaten");
  private static final String jdbcUrlTemplate = "jdbc:%s:%s";

  /**
   * Maakt een volledige jdbc url om connectie met de database te maken.
   *
   * @param sqlDialect Database type waarmee connectie gemaakt gaat worden
   * @param databaseUrl informatie naar welke database connectie gemaakt gaat worden
   * @return complete jdbc connectie url
   */
  public static String databaseUrl(final SQLDialect sqlDialect, final String databaseUrl) {
    return String.format(jdbcUrlTemplate, getDriverName(sqlDialect), databaseUrl);
  }

  /**
   * Geeft de jdbc database naam terug gegeven het {@link SQLDialect}.
   *
   * @param sqlDialect dialect om jdbc naam uit af te leiden
   * @return jdbc database naam
   */
  private static String getDriverName(final SQLDialect sqlDialect) {
    switch (sqlDialect) {
      case SQLITE:
        return "sqlite";
      case POSTGRES:
        return "postgresql";
      default:
        throw new IllegalArgumentException("Unsupported SQL dialect: " + sqlDialect);
    }
  }

  /**
   * Controleert of schema al aanwezig is, en als nog niet aanwezig maakt de tabellen en views aan in de database.
   *
   * @param url url
   * @param gebruikersnaam Optionele gebruikersnaam voor database toegang
   * @param wachtwoord Optionele wachtwoord voor database toegang
   * @throws SQLException
   * @throws IOException
   */
  public static void maakDatabaseSchema(final String url, final String gebruikersnaam, final String wachtwoord) throws SQLException, IOException {
    LOGGER.debug("Database connectie url: {}", url);
    try (Connection con = DriverManager.getConnection(url, gebruikersnaam, wachtwoord)) {
      if (isMetDatabaseTabellen(con)) {
        return;
      }
      final List<String> statements = statements();

      for (final String statement : statements) {
        LOGGER.trace("Statement: {}", statement);
        try (Statement stmt = con.createStatement()) {
          stmt.execute(statement);
        }
      }
    }
  }

  private static List<String> statements() throws IOException {
    final List<String> statements = new ArrayList<>();

    for (final String sqlFile : SQL_FILES) {
      try (final InputStream in = DatabaseMaker.class.getResourceAsStream(sqlFile + ".sql");
          final InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8);
          final BufferedReader br = new BufferedReader(isr)) {
        final String sqlInhoud = br.lines().filter(s -> !s.startsWith("--")).collect(Collectors.joining("\n"));

        statements.addAll(List.of(sqlInhoud.split(";")));
      }
    }
    return statements;
  }

  private static boolean isMetDatabaseTabellen(final Connection con) {
    try {
      DSL.using(con).select(VERKIEZINGEN.VERKIEZING_ID).from(VERKIEZINGEN).limit(1).execute();
      return true;
    } catch (final DataAccessException e) {
      LOGGER.info("Database schema bestaat nog niet, wordt nu aangemaakt.");
      return false;
    }
  }
}
