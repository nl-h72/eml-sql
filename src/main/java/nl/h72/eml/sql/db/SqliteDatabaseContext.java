/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql.db;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Database JOOQ connectie context class voor SQLite.
 */
public class SqliteDatabaseContext extends DatabaseContext {

  private static final Logger LOGGER = LoggerFactory.getLogger(SqliteDatabaseContext.class);

  public SqliteDatabaseContext(final DSLContext dslContext) {
    super(dslContext);
  }

  @Override
  public CompletableFuture<?> batchUitvoeren(final Function<DSLContext, List<Query>> queryLijst) {
    try {
      getDslContext().transaction(configuration -> configuration.dsl().batch(queryLijst.apply(configuration.dsl())).execute());
      return CompletableFuture.completedFuture(Boolean.TRUE);
    } catch (final DataAccessException e) {
      if (e.getMessage().contains("SQLITE_BUSY")) {
        LOGGER.info("SQLite was bezig, we proberen het opnieuw.");
        return batchUitvoeren(queryLijst);
      } else {
        throw e;
      }
    }
  }
}
