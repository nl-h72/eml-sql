/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql.db;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import org.jooq.DSLContext;
import org.jooq.Query;

/**
 * Database JOOQ connectie context class.
 */
public class DatabaseContext {

  private final DSLContext dslContext;

  public DatabaseContext(final DSLContext dslContext) {
    this.dslContext = dslContext;
  }

  public DSLContext getDslContext() {
    return dslContext;
  }

  /**
   * Voer de gegeven lijst met queries asynchroon uit.
   *
   * @param queryLijst lijst met queries
   * @return {@link CompletableFuture} waarmee informatie over de asynchrone uitvoer kan worden opgevraagd.
   */
  public CompletableFuture<?> batchUitvoeren(final Function<DSLContext, List<Query>> queryLijst) {
    return dslContext.transactionAsync(configuration -> configuration.dsl().batch(queryLijst.apply(configuration.dsl())).execute())
        .toCompletableFuture();
  }

  /**
   * Voer de gegeven query uit.
   *
   * @param query query om uit te voeren
   */
  public void transactieUitvoeren(final Function<DSLContext, Query> query) {
    dslContext.transaction(configuration -> configuration.dsl().batch(query.apply(configuration.dsl())).execute());
  }
}
