/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.KANDIDATEN;
import static nl.h72.eml.sql.generated.Tables.KIESKRINGEN;
import static nl.h72.eml.sql.generated.Tables.PARTIJEN;
import static nl.h72.eml.sql.generated.Tables.REGIOS;

import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Bij Europees Parlement verkiezingen zijn geen kiesrkingen. Echter de resultaten worden ook op kiesrkring nivo gepubliceerd.
 * Door ontbrekende kieskring, partij en kandidaat gegevens per kieskring kunnen deze resultaten per kieskring daarom niet in de database worden
 * ingelezen. Om dit toch mogelijk te maken worden de kieskring, partij en kandidaat gegevens aangemaakt door deze te dupliceren van de
 * landelijke gegevens.
 */
class EPPatcher {

  private static final String KIESKRING = "KIESKRING";

  /**
   * Vul kieskring, partij en kandidaat gegevens aan voor Europees Parlement verkiezingen.
   *
   * @param databaseContext
   * @param verkiezingId id van de verkiezing
   */
  public static void patchEPVerkiezingen(final DatabaseContext databaseContext, final String verkiezingId) {
    if (!verkiezingId.startsWith("EP")) {
      return; // Geen Europese verkiezingen, verder niets te doen.
    }
    patchKieskringen(databaseContext, verkiezingId);
    patchPartijen(databaseContext, verkiezingId);
    patchKandidaten(databaseContext, verkiezingId);
  }

  private static void patchKieskringen(final DatabaseContext databaseContext, final String verkiezingId) {
    databaseContext.transactieUitvoeren(
        dslContext -> dslContext.insertInto(KIESKRINGEN, KIESKRINGEN.VERKIEZING_ID, KIESKRINGEN.KIESKRING_ID, KIESKRINGEN.KIESKRING_NAAM)
            .select(dslContext.select(REGIOS.VERKIEZING_ID, REGIOS.REGIO_ID.cast(Integer.class), REGIOS.NAAM)
            .from(REGIOS)
            .where(REGIOS.CATEGORIE.eq(KIESKRING)
            .and(REGIOS.VERKIEZING_ID.eq(verkiezingId)))));
  }

  private static void patchPartijen(final DatabaseContext databaseContext, final String verkiezingId) {
    databaseContext.transactieUitvoeren(
        dslContext -> dslContext
            .insertInto(PARTIJEN, PARTIJEN.VERKIEZING_ID, PARTIJEN.KIESKRING_ID, PARTIJEN.PARTIJ_ID, PARTIJEN.NAAM, PARTIJEN.LIJST_TYPE)
                .select(dslContext.select(REGIOS.VERKIEZING_ID, REGIOS.REGIO_ID.cast(Integer.class), PARTIJEN.PARTIJ_ID, PARTIJEN.NAAM,
                    PARTIJEN.LIJST_TYPE)
                .from(REGIOS, PARTIJEN)
                .where(REGIOS.CATEGORIE.eq(KIESKRING)
                .and(REGIOS.VERKIEZING_ID.eq(verkiezingId)))));
  }

  private static void patchKandidaten(final DatabaseContext databaseContext, final String verkiezingId) {
    databaseContext.transactieUitvoeren(
        dslContext -> dslContext
            .insertInto(KANDIDATEN, KANDIDATEN.VERKIEZING_ID, KANDIDATEN.KIESKRING_ID, KANDIDATEN.PARTIJ_ID, KANDIDATEN.KANDIDAAT_ID,
                KANDIDATEN.KANDIDAAT_CODE, KANDIDATEN.GESLACHT, KANDIDATEN.VOORLETTERS, KANDIDATEN.ROEPNAAM, KANDIDATEN.TUSSENVOEGSEL,
                KANDIDATEN.ACHTERNAAM, KANDIDATEN.WOONPLAATS, KANDIDATEN.LAND_CODE)
                .select(dslContext.select(KANDIDATEN.VERKIEZING_ID, REGIOS.REGIO_ID.cast(Integer.class), KANDIDATEN.PARTIJ_ID,
                    KANDIDATEN.KANDIDAAT_ID, KANDIDATEN.KANDIDAAT_CODE, KANDIDATEN.GESLACHT, KANDIDATEN.VOORLETTERS, KANDIDATEN.ROEPNAAM,
                    KANDIDATEN.TUSSENVOEGSEL, KANDIDATEN.ACHTERNAAM, KANDIDATEN.WOONPLAATS, KANDIDATEN.LAND_CODE)
                .from(REGIOS, KANDIDATEN)
                .where(REGIOS.CATEGORIE.eq(KIESKRING)
                .and(REGIOS.VERKIEZING_ID.eq(verkiezingId)))));
  }
}
