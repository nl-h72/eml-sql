/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import nl.h72.eml.config.ApplicatieConfiguratie;
import nl.h72.eml.config.DatabaseConfiguratie;

@Component
class OpdrachtregelLezer {

  private static final String HELP = "help";

  private static final Map<String, String> HELP_OPTIES = Map.of(
      // @formatter:off
      "--database.url=<database url>", "Pad naar de te gebruiken database",
      "--database.gebruikersnaam=<gebruikersnaam>", "Optioneel database gebruikersnaam",
      "--database.wachtwoord=<wachtwoord>", "Optioneel database wachtwoord",
      "--database.dialect=<dialect", "Database product. 'sqlite' of 'postgres'",
      "--help", "Deze help tekst");
  // @formatter:on
  private static final int KOLOM_MARGE = 5;
  private static final int MAX_KOLOMMEN_OPTIES = HELP_OPTIES.entrySet().stream().mapToInt(e -> e.getKey().length()).max().getAsInt() + KOLOM_MARGE;

  private static final String HELP_TEXT = """
      Gebruik: java -jar eml-sql.<version>.jar [opties] <uitslagen>

      Minimaal moet de database.url en <uitslagen> van de uitslagen worden meegegeven.
      <uitslagen> - Pad naar map met verkiezingsuitslagen.

      Opties:
      %s

      Of maak een application.yaml bestand aan met daarin bovenstaande opties.
      Zie de README.md met de uitleg daarvan.
      """;
  private static final String ARGUMENT_UITSLAGEN_BESTAAT_NIET = "De opgegeven directory '%s' kan niet gevonden worden.";
  private static final String ARGUMENT_UITSLAGEN_NIET_MEEGEGEVEN = "Map met uitslagen niet meegegeven op opdrachtregel.";
  private static final String DATABASE_URL_NIET_MEEGEGEVEN = "Optie 'database.url' niet meegeven of leeg";

  private final ApplicatieConfiguratie applicatieConfiguratie;
  private final DatabaseConfiguratie databaseConfiguratie;

  @Autowired
  public OpdrachtregelLezer(final ApplicatieConfiguratie applicatieConfiguratie, final DatabaseConfiguratie databaseConfiguratie) {
    this.applicatieConfiguratie = applicatieConfiguratie;
    this.databaseConfiguratie = databaseConfiguratie;
  }

  public boolean valideerOpdrachtregelOpties(final ApplicationArguments args) {
    if (args.getOptionValues(HELP) == null) {
      final List<String> nonOptionArgs = args.getNonOptionArgs();
      final String uitslagen = nonOptionArgs.isEmpty() ? "" : nonOptionArgs.get(0);

      if (uitslagen.isBlank()) {
        print(ARGUMENT_UITSLAGEN_NIET_MEEGEGEVEN);
      } else if (!Path.of(uitslagen).toFile().exists()) {
        print(String.format(ARGUMENT_UITSLAGEN_BESTAAT_NIET, uitslagen));
      } else if (databaseConfiguratie.url() == null || databaseConfiguratie.url().isBlank()) {
        print(DATABASE_URL_NIET_MEEGEGEVEN);
      } else {
        return true;
      }
    }
    printHelp();
    return false;
  }

  public void printHelp() {
    final String opties = HELP_OPTIES.entrySet().stream().sorted(Map.Entry.comparingByKey()).map(OpdrachtregelLezer::optieRegel)
        .collect(Collectors.joining(System.lineSeparator()));
    print(String.format(HELP_TEXT, opties));
  }

  private static String optieRegel(final Entry<String, String> e) {
    return e.getKey() + " ".repeat(MAX_KOLOMMEN_OPTIES - e.getKey().length()) + e.getValue();
  }

  @SuppressWarnings("java:S106")
  private static void print(final String regel) {
    System.out.println(regel);
    System.out.println();
  }

  public ConfiguratieRecord leesCommandLineOpties(final ApplicationArguments args) {
    return new ConfiguratieRecord(applicatieConfiguratie, databaseConfiguratie, Path.of(args.getNonOptionArgs().get(0)));
  }
}
