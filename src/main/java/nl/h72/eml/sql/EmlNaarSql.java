/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;

import org.jooq.SQLDialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import nl.h72.eml.config.ApplicatieConfiguratie;
import nl.h72.eml.config.DatabaseConfiguratie;
import nl.h72.eml.filter.NulStemmenFilter;
import nl.h72.eml.omzetten.EmlBestandHulpmiddel.Voorvoegsel;
import nl.h72.eml.omzetten.EmlLezer;
import nl.h72.eml.omzetten.EmlLezer.EmlLezerBouwer;
import nl.h72.eml.omzetten.EmlVerwerker;
import nl.h72.eml.omzetten.ParallelEmlVerwerker;
import nl.h72.eml.omzetten.ParallelEmlVerwerker.ParallelleEmlLezer;
import nl.h72.eml.omzetten.SamengesteldEmlFilter;
import nl.h72.eml.records.LogItem;

@Component
@ComponentScan("nl.h72.eml")
public class EmlNaarSql implements ApplicationRunner {
  private static final Logger LOGGER = LoggerFactory.getLogger(EmlNaarSql.class);

  private final OpdrachtregelLezer opdrachtregelLezer;
  private final SamengesteldEmlFilter filter = new SamengesteldEmlFilter();

  private ConfiguratieRecord configuratie;

  @Autowired
  public EmlNaarSql(final OpdrachtregelLezer opdrachtregelLezer) {
    this.opdrachtregelLezer = opdrachtregelLezer;
  }

  @Override
  public void run(final ApplicationArguments args) throws Exception {
    if (opdrachtregelLezer.valideerOpdrachtregelOpties(args)) {
      configuratie = opdrachtregelLezer.leesCommandLineOpties(args);
      setFilters(configuratie.applicatieConfiguratie());
      LOGGER.debug("Gebruikte configuratie; " + configuratie);
      final DatabaseConfiguratie dbConfiguratie = configuratie.databaseConfiguratie();
      final SQLDialect sqlDialect = dbConfiguratie.sqlDialect();

      vulDatabase(DatabaseMaker.databaseUrl(sqlDialect, dbConfiguratie.url()), sqlDialect, dbConfiguratie.gebruikersnaam(),
          dbConfiguratie.wachtwoord(), configuratie.uitslagenDirectory());
    }
  }

  private void setFilters(final ApplicatieConfiguratie applicatieConfiguratie) {
    if (applicatieConfiguratie.isFilterNulStemmen()) {
      filter.add(new NulStemmenFilter());
    }
  }

  private void vulDatabase(final String url, final SQLDialect sqlDialect, final String databaseGebruikersnaam, final String databaseWachtwoord,
      final Path directory) throws SQLException, IOException {
    DatabaseMaker.maakDatabaseSchema(url, databaseGebruikersnaam, databaseWachtwoord);
    final Instant startTijd = Instant.now();
    final SqlVerwerker bezoeker = new SqlVerwerker(sqlDialect, url, databaseGebruikersnaam, databaseWachtwoord);
    parallelVerwerker(bezoeker, bouwer -> verwerkEml(bouwer, directory, Voorvoegsel.VOORVOEGSEL_230B_BESTANDEN));
    parallelVerwerker(bezoeker, bouwer -> verwerkEml(bouwer, directory, Voorvoegsel.VOORVOEGSEL_110A_BESTANDEN));
    parallelVerwerker(bezoeker, bouwer -> verwerkEml(bouwer, directory, Voorvoegsel.VOORVOEGSEL_510_BESTANDEN));
    parallelVerwerker(bezoeker, bouwer -> verwerkEml(bouwer, directory, Voorvoegsel.VOORVOEGSEL_510_TOTAAL_TELLINGEN_BESTAND));
    parallelVerwerker(bezoeker, bouwer -> verwerkEml(bouwer, directory, Voorvoegsel.VOORVOEGSEL_520_BESTANDEN));
    LOGGER.info("Verwerkingstijd:" + Duration.between(startTijd, Instant.now()).getSeconds() + " seconden.");
  }

  private void parallelVerwerker(final EmlVerwerker bezoeker, final ParallelleEmlLezer parallelleEmlLezer) {
    new ParallelEmlVerwerker(bezoeker, configuratie.applicatieConfiguratie().parallel()).verwerk(parallelleEmlLezer);
  }

  private void verwerkEml(final EmlLezerBouwer bouwer, final Path directory, final Voorvoegsel voorvoegsel) {
    final EmlLezer parallelleLezer = bouwer.logger(EmlNaarSql::logBestand).filter(filter).bouw();

    parallelleLezer.verwerk(directory, f -> Hulpmiddel.startMetVoorvoegsel(f, voorvoegsel.getVoorvoegsel()));
  }

  private static void logBestand(final LogItem logItem) {
    if (logItem.succes()) {
      LOGGER.info(logItem.bericht());
    } else {
      LOGGER.debug(logItem.bericht(), logItem.exception());
    }
  }
}
