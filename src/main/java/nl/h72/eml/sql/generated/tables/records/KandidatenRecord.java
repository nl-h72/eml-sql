/*
 * This file is generated by jOOQ.
 */
package nl.h72.eml.sql.generated.tables.records;


import nl.h72.eml.sql.generated.tables.Kandidaten;

import org.jooq.Field;
import org.jooq.Record12;
import org.jooq.Record4;
import org.jooq.Row12;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class KandidatenRecord extends UpdatableRecordImpl<KandidatenRecord> implements Record12<String, Integer, Integer, Integer, String, String, String, String, String, String, String, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>kandidaten.verkiezing_id</code>.
     */
    public void setVerkiezingId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>kandidaten.verkiezing_id</code>.
     */
    public String getVerkiezingId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>kandidaten.kieskring_id</code>.
     */
    public void setKieskringId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>kandidaten.kieskring_id</code>.
     */
    public Integer getKieskringId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>kandidaten.partij_id</code>.
     */
    public void setPartijId(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>kandidaten.partij_id</code>.
     */
    public Integer getPartijId() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>kandidaten.kandidaat_id</code>.
     */
    public void setKandidaatId(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>kandidaten.kandidaat_id</code>.
     */
    public Integer getKandidaatId() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>kandidaten.kandidaat_code</code>.
     */
    public void setKandidaatCode(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>kandidaten.kandidaat_code</code>.
     */
    public String getKandidaatCode() {
        return (String) get(4);
    }

    /**
     * Setter for <code>kandidaten.geslacht</code>.
     */
    public void setGeslacht(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>kandidaten.geslacht</code>.
     */
    public String getGeslacht() {
        return (String) get(5);
    }

    /**
     * Setter for <code>kandidaten.voorletters</code>.
     */
    public void setVoorletters(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>kandidaten.voorletters</code>.
     */
    public String getVoorletters() {
        return (String) get(6);
    }

    /**
     * Setter for <code>kandidaten.roepnaam</code>.
     */
    public void setRoepnaam(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>kandidaten.roepnaam</code>.
     */
    public String getRoepnaam() {
        return (String) get(7);
    }

    /**
     * Setter for <code>kandidaten.tussenvoegsel</code>.
     */
    public void setTussenvoegsel(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>kandidaten.tussenvoegsel</code>.
     */
    public String getTussenvoegsel() {
        return (String) get(8);
    }

    /**
     * Setter for <code>kandidaten.achternaam</code>.
     */
    public void setAchternaam(String value) {
        set(9, value);
    }

    /**
     * Getter for <code>kandidaten.achternaam</code>.
     */
    public String getAchternaam() {
        return (String) get(9);
    }

    /**
     * Setter for <code>kandidaten.woonplaats</code>.
     */
    public void setWoonplaats(String value) {
        set(10, value);
    }

    /**
     * Getter for <code>kandidaten.woonplaats</code>.
     */
    public String getWoonplaats() {
        return (String) get(10);
    }

    /**
     * Setter for <code>kandidaten.land_code</code>.
     */
    public void setLandCode(String value) {
        set(11, value);
    }

    /**
     * Getter for <code>kandidaten.land_code</code>.
     */
    public String getLandCode() {
        return (String) get(11);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record4<String, Integer, Integer, Integer> key() {
        return (Record4) super.key();
    }

    // -------------------------------------------------------------------------
    // Record12 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row12<String, Integer, Integer, Integer, String, String, String, String, String, String, String, String> fieldsRow() {
        return (Row12) super.fieldsRow();
    }

    @Override
    public Row12<String, Integer, Integer, Integer, String, String, String, String, String, String, String, String> valuesRow() {
        return (Row12) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Kandidaten.KANDIDATEN.VERKIEZING_ID;
    }

    @Override
    public Field<Integer> field2() {
        return Kandidaten.KANDIDATEN.KIESKRING_ID;
    }

    @Override
    public Field<Integer> field3() {
        return Kandidaten.KANDIDATEN.PARTIJ_ID;
    }

    @Override
    public Field<Integer> field4() {
        return Kandidaten.KANDIDATEN.KANDIDAAT_ID;
    }

    @Override
    public Field<String> field5() {
        return Kandidaten.KANDIDATEN.KANDIDAAT_CODE;
    }

    @Override
    public Field<String> field6() {
        return Kandidaten.KANDIDATEN.GESLACHT;
    }

    @Override
    public Field<String> field7() {
        return Kandidaten.KANDIDATEN.VOORLETTERS;
    }

    @Override
    public Field<String> field8() {
        return Kandidaten.KANDIDATEN.ROEPNAAM;
    }

    @Override
    public Field<String> field9() {
        return Kandidaten.KANDIDATEN.TUSSENVOEGSEL;
    }

    @Override
    public Field<String> field10() {
        return Kandidaten.KANDIDATEN.ACHTERNAAM;
    }

    @Override
    public Field<String> field11() {
        return Kandidaten.KANDIDATEN.WOONPLAATS;
    }

    @Override
    public Field<String> field12() {
        return Kandidaten.KANDIDATEN.LAND_CODE;
    }

    @Override
    public String component1() {
        return getVerkiezingId();
    }

    @Override
    public Integer component2() {
        return getKieskringId();
    }

    @Override
    public Integer component3() {
        return getPartijId();
    }

    @Override
    public Integer component4() {
        return getKandidaatId();
    }

    @Override
    public String component5() {
        return getKandidaatCode();
    }

    @Override
    public String component6() {
        return getGeslacht();
    }

    @Override
    public String component7() {
        return getVoorletters();
    }

    @Override
    public String component8() {
        return getRoepnaam();
    }

    @Override
    public String component9() {
        return getTussenvoegsel();
    }

    @Override
    public String component10() {
        return getAchternaam();
    }

    @Override
    public String component11() {
        return getWoonplaats();
    }

    @Override
    public String component12() {
        return getLandCode();
    }

    @Override
    public String value1() {
        return getVerkiezingId();
    }

    @Override
    public Integer value2() {
        return getKieskringId();
    }

    @Override
    public Integer value3() {
        return getPartijId();
    }

    @Override
    public Integer value4() {
        return getKandidaatId();
    }

    @Override
    public String value5() {
        return getKandidaatCode();
    }

    @Override
    public String value6() {
        return getGeslacht();
    }

    @Override
    public String value7() {
        return getVoorletters();
    }

    @Override
    public String value8() {
        return getRoepnaam();
    }

    @Override
    public String value9() {
        return getTussenvoegsel();
    }

    @Override
    public String value10() {
        return getAchternaam();
    }

    @Override
    public String value11() {
        return getWoonplaats();
    }

    @Override
    public String value12() {
        return getLandCode();
    }

    @Override
    public KandidatenRecord value1(String value) {
        setVerkiezingId(value);
        return this;
    }

    @Override
    public KandidatenRecord value2(Integer value) {
        setKieskringId(value);
        return this;
    }

    @Override
    public KandidatenRecord value3(Integer value) {
        setPartijId(value);
        return this;
    }

    @Override
    public KandidatenRecord value4(Integer value) {
        setKandidaatId(value);
        return this;
    }

    @Override
    public KandidatenRecord value5(String value) {
        setKandidaatCode(value);
        return this;
    }

    @Override
    public KandidatenRecord value6(String value) {
        setGeslacht(value);
        return this;
    }

    @Override
    public KandidatenRecord value7(String value) {
        setVoorletters(value);
        return this;
    }

    @Override
    public KandidatenRecord value8(String value) {
        setRoepnaam(value);
        return this;
    }

    @Override
    public KandidatenRecord value9(String value) {
        setTussenvoegsel(value);
        return this;
    }

    @Override
    public KandidatenRecord value10(String value) {
        setAchternaam(value);
        return this;
    }

    @Override
    public KandidatenRecord value11(String value) {
        setWoonplaats(value);
        return this;
    }

    @Override
    public KandidatenRecord value12(String value) {
        setLandCode(value);
        return this;
    }

    @Override
    public KandidatenRecord values(String value1, Integer value2, Integer value3, Integer value4, String value5, String value6, String value7, String value8, String value9, String value10, String value11, String value12) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached KandidatenRecord
     */
    public KandidatenRecord() {
        super(Kandidaten.KANDIDATEN);
    }

    /**
     * Create a detached, initialised KandidatenRecord
     */
    public KandidatenRecord(String verkiezingId, Integer kieskringId, Integer partijId, Integer kandidaatId, String kandidaatCode, String geslacht, String voorletters, String roepnaam, String tussenvoegsel, String achternaam, String woonplaats, String landCode) {
        super(Kandidaten.KANDIDATEN);

        setVerkiezingId(verkiezingId);
        setKieskringId(kieskringId);
        setPartijId(partijId);
        setKandidaatId(kandidaatId);
        setKandidaatCode(kandidaatCode);
        setGeslacht(geslacht);
        setVoorletters(voorletters);
        setRoepnaam(roepnaam);
        setTussenvoegsel(tussenvoegsel);
        setAchternaam(achternaam);
        setWoonplaats(woonplaats);
        setLandCode(landCode);
    }
}
