/*
 * This file is generated by jOOQ.
 */
package nl.h72.eml.sql.generated.tables;


import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import nl.h72.eml.sql.generated.DefaultSchema;
import nl.h72.eml.sql.generated.Keys;
import nl.h72.eml.sql.generated.tables.records.RapportageGebiedenStatistiekenRecord;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function8;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class RapportageGebiedenStatistieken extends TableImpl<RapportageGebiedenStatistiekenRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>rapportage_gebieden_statistieken</code>
     */
    public static final RapportageGebiedenStatistieken RAPPORTAGE_GEBIEDEN_STATISTIEKEN = new RapportageGebiedenStatistieken();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<RapportageGebiedenStatistiekenRecord> getRecordType() {
        return RapportageGebiedenStatistiekenRecord.class;
    }

    /**
     * The column <code>rapportage_gebieden_statistieken.verkiezing_id</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, String> VERKIEZING_ID = createField(DSL.name("verkiezing_id"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>rapportage_gebieden_statistieken.autoriteit_id</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, String> AUTORITEIT_ID = createField(DSL.name("autoriteit_id"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column
     * <code>rapportage_gebieden_statistieken.rapportage_gebied_id</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, String> RAPPORTAGE_GEBIED_ID = createField(DSL.name("rapportage_gebied_id"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>rapportage_gebieden_statistieken.naam</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, String> NAAM = createField(DSL.name("naam"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>rapportage_gebieden_statistieken.type</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, String> TYPE = createField(DSL.name("type"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column
     * <code>rapportage_gebieden_statistieken.stemmen_afgewezen</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, Integer> STEMMEN_AFGEWEZEN = createField(DSL.name("stemmen_afgewezen"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column
     * <code>rapportage_gebieden_statistieken.stemmen_ongeteld</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, Integer> STEMMEN_ONGETELD = createField(DSL.name("stemmen_ongeteld"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>rapportage_gebieden_statistieken.telling</code>.
     */
    public final TableField<RapportageGebiedenStatistiekenRecord, Integer> TELLING = createField(DSL.name("telling"), SQLDataType.INTEGER.nullable(false), this, "");

    private RapportageGebiedenStatistieken(Name alias, Table<RapportageGebiedenStatistiekenRecord> aliased) {
        this(alias, aliased, null);
    }

    private RapportageGebiedenStatistieken(Name alias, Table<RapportageGebiedenStatistiekenRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>rapportage_gebieden_statistieken</code> table
     * reference
     */
    public RapportageGebiedenStatistieken(String alias) {
        this(DSL.name(alias), RAPPORTAGE_GEBIEDEN_STATISTIEKEN);
    }

    /**
     * Create an aliased <code>rapportage_gebieden_statistieken</code> table
     * reference
     */
    public RapportageGebiedenStatistieken(Name alias) {
        this(alias, RAPPORTAGE_GEBIEDEN_STATISTIEKEN);
    }

    /**
     * Create a <code>rapportage_gebieden_statistieken</code> table reference
     */
    public RapportageGebiedenStatistieken() {
        this(DSL.name("rapportage_gebieden_statistieken"), null);
    }

    public <O extends Record> RapportageGebiedenStatistieken(Table<O> child, ForeignKey<O, RapportageGebiedenStatistiekenRecord> key) {
        super(child, key, RAPPORTAGE_GEBIEDEN_STATISTIEKEN);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public UniqueKey<RapportageGebiedenStatistiekenRecord> getPrimaryKey() {
        return Keys.RAPPORTAGE_GEBIEDEN_STATISTIEKEN__RAPPORTAGE_GEBIEDEN_STATISTIEKEN_PKEY;
    }

    @Override
    public List<ForeignKey<RapportageGebiedenStatistiekenRecord, ?>> getReferences() {
        return Arrays.asList(Keys.RAPPORTAGE_GEBIEDEN_STATISTIEKEN__RAPPORTAGE_GEBIEDEN_STATISTIEKEN_FKEY_VERKIEZINGEN);
    }

    private transient Verkiezingen _verkiezingen;

    /**
     * Get the implicit join path to the <code>verkiezingen</code> table.
     */
    public Verkiezingen verkiezingen() {
        if (_verkiezingen == null)
            _verkiezingen = new Verkiezingen(this, Keys.RAPPORTAGE_GEBIEDEN_STATISTIEKEN__RAPPORTAGE_GEBIEDEN_STATISTIEKEN_FKEY_VERKIEZINGEN);

        return _verkiezingen;
    }

    @Override
    public RapportageGebiedenStatistieken as(String alias) {
        return new RapportageGebiedenStatistieken(DSL.name(alias), this);
    }

    @Override
    public RapportageGebiedenStatistieken as(Name alias) {
        return new RapportageGebiedenStatistieken(alias, this);
    }

    @Override
    public RapportageGebiedenStatistieken as(Table<?> alias) {
        return new RapportageGebiedenStatistieken(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public RapportageGebiedenStatistieken rename(String name) {
        return new RapportageGebiedenStatistieken(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public RapportageGebiedenStatistieken rename(Name name) {
        return new RapportageGebiedenStatistieken(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public RapportageGebiedenStatistieken rename(Table<?> name) {
        return new RapportageGebiedenStatistieken(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row8<String, String, String, String, String, Integer, Integer, Integer> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function8<? super String, ? super String, ? super String, ? super String, ? super String, ? super Integer, ? super Integer, ? super Integer, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function8<? super String, ? super String, ? super String, ? super String, ? super String, ? super Integer, ? super Integer, ? super Integer, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}
