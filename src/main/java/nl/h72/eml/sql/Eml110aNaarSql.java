/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.KIESKRINGEN;
import static nl.h72.eml.sql.generated.Tables.PARTIJEN;
import static nl.h72.eml.sql.generated.Tables.REGIOS;
import static nl.h72.eml.sql.generated.Tables.VERKIEZINGEN;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jooq.DSLContext;
import org.jooq.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.ivu.wahl.wus.oasis.eml.kiesraad.CommitteeCategoryType;
import de.ivu.wahl.wus.oasis.eml.kiesraad.RegionCategoryType;

import nl.h72.eml.records.Eml110a;
import nl.h72.eml.records.Regio;
import nl.h72.eml.records.Verkiezing;
import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Zet EML110a bestanden in de database.
 */
class Eml110aNaarSql {
  private final DSLContext dslContext;
  private final Eml110a eml110a;

  private Eml110aNaarSql(final DSLContext dslContext, final Eml110a eml110a) {
    this.dslContext = dslContext;
    this.eml110a = eml110a;
  }

  @Transactional(propagation = Propagation.MANDATORY)
  public static void voegInDatabase(final DatabaseContext databaseContext, final Eml110a eml110a) {
    databaseContext.transactieUitvoeren(dslContext -> updateVerkiezingIn(dslContext, eml110a));
    databaseContext.batchUitvoeren(dslContext -> {
      final Eml110aNaarSql batcher = new Eml110aNaarSql(dslContext, eml110a);

      return batcher.voegRegiosIn();
    }).join();
    EPPatcher.patchEPVerkiezingen(databaseContext, eml110a.verkiezing().verkiezingId());
  }

  private static Query updateVerkiezingIn(final DSLContext dslContext, final Eml110a eml110a) {
    final Verkiezing verkiezing = eml110a.verkiezing();

    return dslContext.update(VERKIEZINGEN).set(VERKIEZINGEN.AANTAL_ZETELS, eml110a.aantalZetels())
        .set(VERKIEZINGEN.VOORKEURSDREMPEL, eml110a.voorkeursdrempel()).where(VERKIEZINGEN.VERKIEZING_ID.eq(verkiezing.verkiezingId()));
  }

  private List<Query> voegRegiosIn() {
    final String verkiezingId = eml110a.verkiezing().verkiezingId();

    return Stream.concat(eml110a.regios().stream().map(regio -> voegRegioIn(verkiezingId, regio)),
        voegGeregistreerdePartijIn(verkiezingId, eml110a.kieskringId(), eml110a.verkiezing().naam(), eml110a.geregistreerdePartijen())).toList();
  }

  private Query voegRegioIn(final String verkiezingId, final Regio regio) {
    return dslContext.insertInto(REGIOS, REGIOS.VERKIEZING_ID, REGIOS.REGIO_ID, REGIOS.NAAM, REGIOS.CATEGORIE, REGIOS.COMMISSIE_CATEGORIE,
        REGIOS.OUDER_REGIO_ID, REGIOS.OUDER_REGIO_CATEGORIE, REGIOS.FRIESE_EXPORT_TOEGESTAAN, REGIOS.ROMEINSE_CIJFERS).values(verkiezingId,
            String.valueOf(regio.regioNummer()), regio.naam(), regioCategorie(regio.categorie()), regioCommissieCategorie(regio.commissieCategorie()),
            String.valueOf(regio.ouderRegioNummer()), regioCategorie(regio.ouderRegioCategorie()),
            SqlHulpmiddelen.b2i(regio.frieseExportToegestaan()), SqlHulpmiddelen.b2i(regio.romeinseCijfers()));
  }

  private static String regioCommissieCategorie(final CommitteeCategoryType committeeCategoryType) {
    return committeeCategoryType == null ? null : committeeCategoryType.name();
  }

  private static String regioCategorie(final RegionCategoryType regionCategoryType) {
    return regionCategoryType == null ? null : regionCategoryType.name();
  }

  private Stream<Query> voegGeregistreerdePartijIn(final String verkiezingId, final String kieskring, final String kieskringNaam,
      final List<String> geregistreerdePartijen) {
    if ("geen".equals(kieskring)) {
      return Stream.of();
    }
    final Integer kieskringId = Hulpmiddel.kieskringId(kieskring, null);

    return Stream.concat(Stream.of(voegKieskringIn(verkiezingId, kieskringId, kieskringNaam)),
        IntStream.range(0, geregistreerdePartijen.size())
        .mapToObj(i -> dslContext
            .insertInto(PARTIJEN, PARTIJEN.VERKIEZING_ID, PARTIJEN.KIESKRING_ID, PARTIJEN.PARTIJ_ID, PARTIJEN.NAAM, PARTIJEN.LIJST_TYPE)
            .values(verkiezingId, kieskringId, i + 1, geregistreerdePartijen.get(i), "")
            .onDuplicateKeyIgnore()));
  }

  private Query voegKieskringIn(final String verkiezingId, final Integer kieskringId, final String naam) {
    return dslContext.insertInto(KIESKRINGEN, KIESKRINGEN.VERKIEZING_ID, KIESKRINGEN.KIESKRING_ID, KIESKRINGEN.KIESKRING_NAAM)
        .values(verkiezingId, kieskringId, naam)
        .onDuplicateKeyIgnore();
  }
}
