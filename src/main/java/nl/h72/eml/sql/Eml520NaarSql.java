/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.GEKOZEN_KANDIDATEN;
import static nl.h72.eml.sql.generated.Tables.KANDIDATEN;

import java.util.List;
import java.util.stream.Stream;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.impl.DSL;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import nl.h72.eml.records.Eml520;
import nl.h72.eml.records.GekozenKandidaat;
import nl.h72.eml.records.GekozenPartij;
import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Zet EML520 bestanden in de database.
 */
class Eml520NaarSql {

  private final DSLContext dslContext;

  private Eml520NaarSql(final DSLContext dslContext) {
    this.dslContext = dslContext;
  }

  @Transactional(propagation = Propagation.MANDATORY)
  public static void voegInDatabaseIn(final DatabaseContext databaseContext, final Eml520 eml520) {
    databaseContext.batchUitvoeren(dslContext -> new Eml520NaarSql(dslContext).voegInDatabaseIn(eml520)).join();
  }

  private List<Query> voegInDatabaseIn(final Eml520 eml520) {
    final Integer alternatiefKieskringId = eml520.verkiezing().verkiezingsDomeinId();

    return eml520.gekozenPartijen().stream().flatMap(gp -> voegGekozenPartijIn(eml520.verkiezing().verkiezingId(), gp, alternatiefKieskringId))
        .toList();
  }

  private Stream<Query> voegGekozenPartijIn(final String verkiezingId, final GekozenPartij gekozenPartij, final Integer alternatiefKieskringId) {
    final Integer kieskringId = Hulpmiddel.kieskringId(gekozenPartij.kieskringId(), alternatiefKieskringId);

    return gekozenPartij.gekozenKandidaten().stream()
        .map(k -> voegKandidaatIn(verkiezingId, kieskringId, gekozenPartij.partijId(), gekozenPartij.naam(), k));
  }

  private Query voegKandidaatIn(final String verkiezingId, final Integer kieskringId, final int partijId, final String partijNaam,
      final GekozenKandidaat kandidaat) {
    return dslContext
        .insertInto(GEKOZEN_KANDIDATEN, GEKOZEN_KANDIDATEN.VERKIEZING_ID, GEKOZEN_KANDIDATEN.KIESKRING_ID, GEKOZEN_KANDIDATEN.PARTIJ_ID,
            GEKOZEN_KANDIDATEN.PARTIJ_NAAM, GEKOZEN_KANDIDATEN.KANDIDAAT_ID, GEKOZEN_KANDIDATEN.GESLACHT, GEKOZEN_KANDIDATEN.VOORLETTERS,
            GEKOZEN_KANDIDATEN.ROEPNAAM, GEKOZEN_KANDIDATEN.TUSSENVOEGSEL, GEKOZEN_KANDIDATEN.ACHTERNAAM, GEKOZEN_KANDIDATEN.WOONPLAATS,
            GEKOZEN_KANDIDATEN.LAND_CODE, GEKOZEN_KANDIDATEN.GEKOZEN, GEKOZEN_KANDIDATEN.RANGSCHIKKING)
        .select(dslContext.selectDistinct(DSL.inline(verkiezingId).as(GEKOZEN_KANDIDATEN.VERKIEZING_ID),
            DSL.inline(kieskringId).as(GEKOZEN_KANDIDATEN.KIESKRING_ID), DSL.inline(partijId).as(GEKOZEN_KANDIDATEN.PARTIJ_ID),
            DSL.inline(partijNaam).as(GEKOZEN_KANDIDATEN.PARTIJ_NAAM), KANDIDATEN.KANDIDAAT_ID,
            DSL.inline(kandidaat.geslacht()).as(GEKOZEN_KANDIDATEN.GESLACHT), DSL.inline(kandidaat.voorletters()).as(GEKOZEN_KANDIDATEN.VOORLETTERS),
            DSL.inline(kandidaat.roepnaam()).as(GEKOZEN_KANDIDATEN.ROEPNAAM),
            DSL.inline(kandidaat.tussenvoegsel()).as(GEKOZEN_KANDIDATEN.TUSSENVOEGSEL),
            DSL.inline(kandidaat.achternaam()).as(GEKOZEN_KANDIDATEN.ACHTERNAAM),
            DSL.inline(kandidaat.woonplaats()).as(GEKOZEN_KANDIDATEN.WOONPLAATS),
            DSL.inline(kandidaat.landCode()).as(GEKOZEN_KANDIDATEN.LAND_CODE),
            DSL.inline(SqlHulpmiddelen.b2i(kandidaat.gekozen())).as(GEKOZEN_KANDIDATEN.GEKOZEN),
            DSL.inline(kandidaat.rangschikking()).as(GEKOZEN_KANDIDATEN.RANGSCHIKKING)).from(KANDIDATEN)
            .where(kandidaatIdOfCode(kandidaat))
            .and(KANDIDATEN.PARTIJ_ID.eq(partijId))
            .and(KANDIDATEN.VERKIEZING_ID.eq(verkiezingId))
            .and(KANDIDATEN.KIESKRING_ID.eq(kieskringId)));
  }

  private static Condition kandidaatIdOfCode(final GekozenKandidaat kandidaat) {
    return kandidaat.kandidaatCode() == null
        ? KANDIDATEN.KANDIDAAT_ID.eq(kandidaat.kandidaatId())
        : KANDIDATEN.KANDIDAAT_CODE.eq(kandidaat.kandidaatCode());
  }
}
