/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import org.jooq.DSLContext;
import org.jooq.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;
import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Zet 510 bestanden in de database. Maar niet de Totaaltellingen bestanden.
 */
class Eml510NaarSql extends Eml510NaarSqlBasis {
  private static final Logger LOGGER = LoggerFactory.getLogger(Eml510NaarSql.class);

  private Eml510NaarSql(final DSLContext dslContext, final Eml510 eml510, final RapportageGebied rapportageGebied) {
    super(dslContext, eml510, rapportageGebied);
  }

  @Transactional(propagation = Propagation.MANDATORY)
  public static void voegInDatabase(final DatabaseContext databaseContext, final Eml510 eml510) {
    final Map<String, String> uniekStembureauNaamMap = new HashMap<>();

    databaseContext
        .batchUitvoeren(
            dslContext -> new Eml510NaarSql(dslContext, eml510, eml510.rapportageGebiedTotaal()).voegRapportageGebiedenIn(uniekStembureauNaamMap))
        .toCompletableFuture().join();

    eml510.rapportageGebieden().stream()
        .map(rg -> databaseContext
            .batchUitvoeren(dslContext -> new Eml510NaarSql(dslContext, eml510, rg).voegRapportageGebiedenIn(uniekStembureauNaamMap)))
        .forEach(CompletableFuture::join);
  }

  private List<Query> voegRapportageGebiedenIn(final Map<String, String> uniekStembureauNaamMap) {
    return voegRapportageGebiedIn(alternatiefKieskringId, rapportageGebied, uniekStembureauNaamMap).toList();
  }

  private Stream<Query> voegRapportageGebiedIn(final Integer alternatiefKieskringId, final RapportageGebied rg,
      final Map<String, String> uniekStembureauNaamMap) {
    final String rapportageGebiedId = uniekRapportageGebiedId(uniekStembureauNaamMap, rg.id());

    return voegRapportageGebiedIn(alternatiefKieskringId, rg, rapportageGebiedId);
  }

  private static String uniekRapportageGebiedId(final Map<String, String> uniekStembureauNaamMap, final String id) {
    int i = 0;
    String uniekeId = id;

    while (uniekStembureauNaamMap.containsKey(uniekeId)) {
      uniekeId = id + '-' + (char) ('a' + i++);
      LOGGER.info("Hernummer {} naar {}", id, uniekeId);
    }
    uniekStembureauNaamMap.putIfAbsent(uniekeId, id);
    return uniekeId;
  }

  @Override
  protected Stream<Query> voegPartijStemmenToe(final Integer kieskringId, final String rapportageGebiedId, final PartijStemmen ps) {
    final Stream<Query> partij = voegInPartijStemmenQuery(kieskringId, rapportageGebiedId, ps);

    return Stream.concat(partij, ps.kandidatenStemmen().stream()
        .map(ks -> voegKandidaatStemmenToe(kieskringId, rapportageGebiedId, ps.partijId(), ks.kandidaatId(), ks.aantalStemmen())));
  }
}
