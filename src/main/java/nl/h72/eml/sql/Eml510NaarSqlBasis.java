/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.KANDIDATEN_STEMMEN;
import static nl.h72.eml.sql.generated.Tables.PARTIJEN_STEMMEN;
import static nl.h72.eml.sql.generated.Tables.RAPPORTAGE_GEBIEDEN;
import static nl.h72.eml.sql.generated.Tables.RAPPORTAGE_GEBIEDEN_STATISTIEKEN;

import java.math.BigInteger;
import java.util.Optional;
import java.util.stream.Stream;

import org.jooq.DSLContext;
import org.jooq.Query;

import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;
import nl.h72.eml.records.StemStatistieken;

/**
 * Basis class voor EML510 bestanden.
 */
abstract class Eml510NaarSqlBasis {

  protected final DSLContext dslContext;
  protected final String emlId;
  protected final String verkiezingId;
  protected final String autoriteitId;
  protected final Integer alternatiefKieskringId;
  protected final RapportageGebied rapportageGebied;

  protected Eml510NaarSqlBasis(final DSLContext dslContext, final Eml510 eml510, final RapportageGebied rapportageGebied) {
    this.dslContext = dslContext;
    this.emlId = eml510.emlId();
    this.verkiezingId = eml510.verkiezing().verkiezingId();
    this.autoriteitId = eml510.autoriteit().id();
    this.alternatiefKieskringId = eml510.verkiezing().verkiezingsDomeinId();
    this.rapportageGebied = rapportageGebied;
  }

  protected Query voegRapportageGebiedIn(final RapportageGebied rg, final String rapportageGebiedId) {
    return dslContext
        .insertInto(RAPPORTAGE_GEBIEDEN, RAPPORTAGE_GEBIEDEN.VERKIEZING_ID, RAPPORTAGE_GEBIEDEN.AUTORITEIT_ID,
            RAPPORTAGE_GEBIEDEN.RAPPORTAGE_GEBIED_ID, RAPPORTAGE_GEBIEDEN.EML_ID, RAPPORTAGE_GEBIEDEN.NAAM, RAPPORTAGE_GEBIEDEN.POSTCODE)
        .values(verkiezingId, autoriteitId, rapportageGebiedId, emlId, rg.naam(), rg.postcode()).onDuplicateKeyIgnore();
  }

  protected Stream<Query> voegStemStatistiekenIn(final String rapportageGebiedId, final StemStatistieken stemStatistieken) {
    return stemStatistieken.stemmenTypeTellingen().entrySet().stream()
        .map(st -> dslContext.insertInto(RAPPORTAGE_GEBIEDEN_STATISTIEKEN, RAPPORTAGE_GEBIEDEN_STATISTIEKEN.VERKIEZING_ID,
            RAPPORTAGE_GEBIEDEN_STATISTIEKEN.AUTORITEIT_ID, RAPPORTAGE_GEBIEDEN_STATISTIEKEN.RAPPORTAGE_GEBIED_ID,
            RAPPORTAGE_GEBIEDEN_STATISTIEKEN.NAAM, RAPPORTAGE_GEBIEDEN_STATISTIEKEN.TYPE, RAPPORTAGE_GEBIEDEN_STATISTIEKEN.STEMMEN_AFGEWEZEN,
            RAPPORTAGE_GEBIEDEN_STATISTIEKEN.STEMMEN_ONGETELD, RAPPORTAGE_GEBIEDEN_STATISTIEKEN.TELLING).values(verkiezingId, autoriteitId,
                rapportageGebiedId, st.getKey().getReasonCode(), st.getKey().name(), SqlHulpmiddelen.b2i(st.getKey().isRejectedVote()),
                SqlHulpmiddelen.b2i(st.getKey().isUncountedVote()), Optional.ofNullable(st.getValue()).map(BigInteger::intValue).orElse(0)));
  }

  protected Stream<Query> voegRapportageGebiedIn(final Integer alternatiefKieskringId, final RapportageGebied rg, final String rapportageGebiedId) {
    final Integer kieskringId = Hulpmiddel.kieskringId(rg.kieskringId(), alternatiefKieskringId);
    final Query rapportageGebied = voegRapportageGebiedIn(rg, rapportageGebiedId);
    final Stream<Query> stemStatistieken = voegStemStatistiekenIn(rapportageGebiedId, rg.statistieken());

    return Stream.concat(Stream.concat(Stream.of(rapportageGebied), stemStatistieken),
        rg.partijenStemmen().stream().flatMap(ps -> voegPartijStemmenToe(kieskringId, rapportageGebiedId, ps)));
  }

  protected abstract Stream<Query> voegPartijStemmenToe(final Integer kieskringId, final String rapportageGebiedId, final PartijStemmen ps);

  protected Stream<Query> voegInPartijStemmenQuery(final Integer kieskringId, final String rapportageGebiedId, final PartijStemmen ps) {
    return Stream.of(dslContext
        .insertInto(PARTIJEN_STEMMEN, PARTIJEN_STEMMEN.VERKIEZING_ID, PARTIJEN_STEMMEN.KIESKRING_ID, PARTIJEN_STEMMEN.AUTORITEIT_ID,
            PARTIJEN_STEMMEN.RAPPORTAGE_GEBIED_ID, PARTIJEN_STEMMEN.PARTIJ_ID, PARTIJEN_STEMMEN.NAAM, PARTIJEN_STEMMEN.AANTAL_STEMMEN)
        .values(verkiezingId, kieskringId, autoriteitId, rapportageGebiedId, ps.partijId(), ps.naam(), ps.aantalStemmen()));
  }

  protected Query voegKandidaatStemmenToe(final Integer kieskringId, final String rapportageGebiedId, final int partijId, final int kandidaatId,
      final Integer aantalStemmen) {
    return dslContext
        .insertInto(KANDIDATEN_STEMMEN, KANDIDATEN_STEMMEN.VERKIEZING_ID, KANDIDATEN_STEMMEN.AUTORITEIT_ID, KANDIDATEN_STEMMEN.KIESKRING_ID,
            KANDIDATEN_STEMMEN.RAPPORTAGE_GEBIED_ID, KANDIDATEN_STEMMEN.PARTIJ_ID, KANDIDATEN_STEMMEN.KANDIDAAT_ID, KANDIDATEN_STEMMEN.AANTAL_STEMMEN)
        .values(verkiezingId, autoriteitId, kieskringId, rapportageGebiedId, partijId, kandidaatId, aantalStemmen);
  }
}
