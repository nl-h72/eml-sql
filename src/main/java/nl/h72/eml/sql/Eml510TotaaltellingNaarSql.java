/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.KANDIDATEN;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.impl.DSL;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;
import nl.h72.eml.sql.db.DatabaseContext;

/**
 * Class voor het inlezen van Totaaltellingen bestand. In dit bestand moeten kandidaten op basis van kandidaat code (ShortCode) wordt geidentificeerd
 * omdat het id in totalen niet aanwezig is, en per kieskring Dit bestand is specifiek voor Tweede Kamer verkiezingen.
 */
class Eml510TotaaltellingNaarSql extends Eml510NaarSqlBasis {
  private static final Pattern HSB_PATROON = Pattern.compile("HSB(\\d+)");

  private Eml510TotaaltellingNaarSql(final DSLContext dslContext, final Eml510 eml510, final RapportageGebied rapportageGebied) {
    super(dslContext, eml510, rapportageGebied);
  }

  @Transactional(propagation = Propagation.MANDATORY)
  public static void voegInDatabase(final DatabaseContext databaseContext, final Eml510 eml510) {
    eml510.rapportageGebieden().stream()
        .map(rg -> databaseContext.batchUitvoeren(dslContext -> new Eml510TotaaltellingNaarSql(dslContext, eml510, rg).voegRapportageGebiedenIn()))
        .forEach(CompletableFuture::join);
    // Sla totalen op nadat per HSB gedaan is, omdat die de kandidaat code update van kandidaten, wat nodig is voor kandidaten totalen.
    databaseContext
        .batchUitvoeren(dslContext -> new Eml510TotaaltellingNaarSql(dslContext, eml510, eml510.rapportageGebiedTotaal()).voegRapportageGebiedenIn())
        .toCompletableFuture().join();
  }

  private List<Query> voegRapportageGebiedenIn() {
    return voegRapportageGebiedIn(alternatiefKieskringId, rapportageGebied, rapportageGebied.id()).toList();
  }

  @Override
  protected Stream<Query> voegPartijStemmenToe(final Integer kieskringId, final String rapportageGebiedId, final PartijStemmen ps) {
    final Stream<Query> partij = voegInPartijStemmenQuery(kieskringId, rapportageGebiedId, ps);
    final Matcher matcher = HSB_PATROON.matcher(rapportageGebiedId);

    return Stream.concat(partij, matcher.find() ? updateKandidaatStemmen(Integer.parseInt(matcher.group(1)), rapportageGebiedId, ps)
        : voegToeKandidaatStemmen(kieskringId, rapportageGebiedId, ps));
  }

  private Stream<Query> updateKandidaatStemmen(final int kieskringId, final String rapportageGebiedId, final PartijStemmen ps) {
    return ps.kandidatenStemmen().stream().map(ks -> updateKandidaat(kieskringId, rapportageGebiedId, ps.partijId(), ks, ks.kandidaatId()))
        .flatMap(Function.identity());
  }

  private Stream<Query> updateKandidaat(final int kieskringId, final String rapportageGebiedId, final int partijId,
      final KandidaatStemmen kandidaatStemmen, final int kandidaatId) {
    return Stream.of(
        dslContext.update(KANDIDATEN).set(KANDIDATEN.KANDIDAAT_CODE, kandidaatStemmen.kandidaatCode())
            .where(KANDIDATEN.VERKIEZING_ID.eq(verkiezingId).and(KANDIDATEN.KIESKRING_ID.eq(kieskringId)).and(KANDIDATEN.PARTIJ_ID.eq(partijId))
                .and(KANDIDATEN.KANDIDAAT_ID.eq(kandidaatId))),
        voegKandidaatStemmenToe(kieskringId, rapportageGebiedId, partijId, kandidaatId, kandidaatStemmen.aantalStemmen()));
  }

  private Stream<Query> voegToeKandidaatStemmen(final Integer kieskringId, final String rapportageGebiedId, final PartijStemmen ps) {
    final List<KandidaatStemmen> ks = ps.kandidatenStemmen();

    return IntStream.range(0, ks.size()).mapToObj(i -> voegKandidaatIn(kieskringId, rapportageGebiedId, ps.partijId(), ks.get(i), i + 1))
        .flatMap(Function.identity());
  }

  /**
   * Kandidaten alleen toevoegen voor kieskring 0. Andere staan er al in. kandidaten id kieskring 0 en gekozen kandidaten id matchen, maar id komt
   * niet overeen met kandidaat id per kieskring :-(
   *
   * @param kieskringId
   * @param rapportageGebiedId
   * @param partijId
   * @param kandidaatStemmen
   * @param kandidaatId
   * @return
   */
  private Stream<Query> voegKandidaatIn(final int kieskringId, final String rapportageGebiedId, final int partijId,
      final KandidaatStemmen kandidaatStemmen, final int kandidaatId) {
    return Stream.of(
        dslContext
            .insertInto(KANDIDATEN, KANDIDATEN.VERKIEZING_ID, KANDIDATEN.KIESKRING_ID, KANDIDATEN.PARTIJ_ID, KANDIDATEN.KANDIDAAT_ID,
                KANDIDATEN.KANDIDAAT_CODE, KANDIDATEN.GESLACHT, KANDIDATEN.VOORLETTERS, KANDIDATEN.ROEPNAAM, KANDIDATEN.TUSSENVOEGSEL,
                KANDIDATEN.ACHTERNAAM, KANDIDATEN.WOONPLAATS)
            .select(dslContext
                .selectDistinct(KANDIDATEN.VERKIEZING_ID, DSL.inline(kieskringId).as(KANDIDATEN.KIESKRING_ID), KANDIDATEN.PARTIJ_ID,
                    DSL.inline(kandidaatId).as(KANDIDATEN.KANDIDAAT_ID), DSL.inline(kandidaatStemmen.kandidaatCode()).as(KANDIDATEN.KANDIDAAT_CODE),
                    KANDIDATEN.GESLACHT, KANDIDATEN.VOORLETTERS, KANDIDATEN.ROEPNAAM, KANDIDATEN.TUSSENVOEGSEL, KANDIDATEN.ACHTERNAAM,
                    KANDIDATEN.WOONPLAATS)
                .from(KANDIDATEN).where(KANDIDATEN.VERKIEZING_ID.eq(verkiezingId)).and(KANDIDATEN.PARTIJ_ID.eq(partijId))
                .and(KANDIDATEN.KANDIDAAT_CODE.equal(kandidaatStemmen.kandidaatCode()))),
        voegKandidaatStemmenToe(kieskringId, rapportageGebiedId, partijId, kandidaatId, kandidaatStemmen.aantalStemmen()));
  }
}
