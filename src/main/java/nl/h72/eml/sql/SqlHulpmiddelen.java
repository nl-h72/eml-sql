/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import static nl.h72.eml.sql.generated.Tables.KIESKRINGEN;
import static nl.h72.eml.sql.generated.Tables.PARTIJEN;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.function.Consumer;

import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import nl.h72.eml.records.Partij;
import nl.h72.eml.sql.db.DatabaseContext;
import nl.h72.eml.sql.db.SqliteDatabaseContext;

/**
 * Hulpmiddelen class gerelateerd aan SQL.
 */
final class SqlHulpmiddelen {

  private SqlHulpmiddelen() {
    // Hulpmiddelen class
  }

  /**
   * Converteert een boolean waarde naar een int waarde om te gebruiken in database tabellen.
   *
   * @param b boolean
   * @return int representatie van de boolean
   */
  public static int b2i(final boolean b) {
    return b ? 1 : 0;
  }

  public static void wrapDatabaseConnectie(final Consumer<DatabaseContext> consument, final SQLDialect sqlDialect, final String url,
      final String gebruikersnaam, final String wachtwoord) throws SQLException {
    try (final Connection con = DriverManager.getConnection(url, gebruikersnaam, wachtwoord)) {
      final DSLContext dslContext = DSL.using(con, sqlDialect);

      consument.accept(sqlDialect == SQLDialect.SQLITE ? new SqliteDatabaseContext(dslContext) : new DatabaseContext(dslContext));
    }
  }

  public static Query voegKieskringIn(final DSLContext dslContext, final String verkiezingId, final String kieskringNaam,
      final Integer kieskringId) {
    return dslContext.insertInto(KIESKRINGEN, KIESKRINGEN.VERKIEZING_ID, KIESKRINGEN.KIESKRING_ID, KIESKRINGEN.KIESKRING_NAAM)
        .values(verkiezingId, kieskringId, kieskringNaam)
        .onDuplicateKeyIgnore();
  }

  public static Query voegPartijIn(final DSLContext dslContext, final String verkiezingId, final Integer kieskringId, final Partij partij) {
    return dslContext.insertInto(PARTIJEN, PARTIJEN.VERKIEZING_ID, PARTIJEN.KIESKRING_ID, PARTIJEN.PARTIJ_ID, PARTIJEN.NAAM, PARTIJEN.LIJST_TYPE)
        .values(verkiezingId, kieskringId, partij.partijId(), partij.naam(), partij.lijstType().getValue())
        .onDuplicateKeyIgnore();
  }
}
