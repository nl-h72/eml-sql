/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import java.io.File;

/**
 * Hulpmiddel class.
 */
final class Hulpmiddel {

  private Hulpmiddel() {
  }

  /**
   * Zet een kieskring id uit een 'contest' om naar een getal. Als er geen kieskring id is staat er "geen", dit wordt dan omgezet naar null. Als het
   * een Romeins cijfer is geen de numerieke waarde terug.
   *
   * @param kieskringId kieskring Id
   * @param geenIdAlternatief
   * @return getal van kieskring of null indien geen kieskring
   */
  public static Integer kieskringId(final String kieskringId, final Integer geenIdAlternatief) {
    switch (kieskringId) {
      case "alle":
        return 0;
      case "geen":
        return geenIdAlternatief == null ? 0 : geenIdAlternatief;
      case "I":
        return 1;
      case "II":
        return 2;
      case "III":
        return 3;
      default:
        return Integer.valueOf(kieskringId);
    }
  }

  /**
   * Test of de bestandsnaam van het gegeven bestand begint met de gegeven voorvoegsel.
   *
   * @param bestand bestand waar de naam van gecontroleerd wordt
   * @param voorvoegsel om te controleren
   * @return true als bestandsnaam begint met het gegeven voorvoegsel
   */
  public static boolean startMetVoorvoegsel(final File bestand, final String voorvoegsel) {
    return bestand.getName().startsWith(voorvoegsel);
  }
}
