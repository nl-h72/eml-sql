/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.sql;

import java.io.File;
import java.sql.SQLException;
import java.util.function.Consumer;

import org.jooq.SQLDialect;

import nl.h72.eml.omzetten.EmlBestandHulpmiddel.Voorvoegsel;
import nl.h72.eml.omzetten.EmlVerwerker;
import nl.h72.eml.records.Eml110a;
import nl.h72.eml.records.Eml230b;
import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.Eml520;
import nl.h72.eml.sql.db.DatabaseContext;

/**
 * {@link EmlVerwerker} implementatie om gegevens in de database op te slaan.
 */
class SqlVerwerker implements EmlVerwerker {

  private final SQLDialect sqlDialect;
  private final String url;
  private final String gebruikersnaam;
  private final String wachtwoord;

  public SqlVerwerker(final SQLDialect sqlDialect, final String url, final String gebruikersnaam, final String wachtwoord) {
    this.sqlDialect = sqlDialect;
    this.url = url;
    this.gebruikersnaam = gebruikersnaam;
    this.wachtwoord = wachtwoord;
  }

  @Override
  public void verwerk(final File bestand, final Eml110a eml110a) throws SQLException {
    wrapDatabaseConnectie(dslContext -> Eml110aNaarSql.voegInDatabase(dslContext, eml110a));
  }

  @Override
  public void verwerk(final File bestand, final Eml230b eml230) throws SQLException {
    wrapDatabaseConnectie(dslContext -> Eml230bNaarSql.voegInDatabase(dslContext, eml230));
  }

  @Override
  public void verwerk(final File bestand, final Eml510 eml510) throws SQLException {
    wrapDatabaseConnectie(databaseContext -> {
      if (Hulpmiddel.startMetVoorvoegsel(bestand, Voorvoegsel.VOORVOEGSEL_510_TOTAAL_TELLINGEN_BESTAND.getVoorvoegsel())) {
        Eml510TotaaltellingNaarSql.voegInDatabase(databaseContext, eml510);
      } else {
        Eml510NaarSql.voegInDatabase(databaseContext, eml510);
      }
    });
  }

  @Override
  public void verwerk(final File bestand, final Eml520 eml520) throws SQLException {
    wrapDatabaseConnectie(dslContext -> Eml520NaarSql.voegInDatabaseIn(dslContext, eml520));
  }

  public void runQuery(final String query) throws SQLException {
    wrapDatabaseConnectie(dslContext -> dslContext.getDslContext().batch(query).execute());
  }
  private void wrapDatabaseConnectie(final Consumer<DatabaseContext> consument) throws SQLException {
    SqlHulpmiddelen.wrapDatabaseConnectie(consument, sqlDialect, url, gebruikersnaam, wachtwoord);
  }
}
