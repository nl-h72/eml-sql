/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Generieke applicatie configuratie.
 */
@ConfigurationProperties
@Component
public class ApplicatieConfiguratie {
  private int parallel = 1;
  private boolean filterNulStemmen;

  public int parallel() {
    return parallel;
  }

  public void setParallel(final int parallel) {
    this.parallel = parallel;
  }

  public void setFilterNulStemmen(final boolean filterNulStemmen) {
    this.filterNulStemmen = filterNulStemmen;
  }

  public boolean isFilterNulStemmen() {
    return filterNulStemmen;
  }
}
