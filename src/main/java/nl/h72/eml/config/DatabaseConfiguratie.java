/*
 * Copyright 2024 EML-SQL bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 */
package nl.h72.eml.config;

import java.util.Locale;

import org.jooq.SQLDialect;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Database configuratie.
 */
@ConfigurationProperties(prefix = "database")
@Component
public class DatabaseConfiguratie {
  private String url;
  private String gebruikersnaam;
  private String wachtwoord;
  private String dialect;

  public String url() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String gebruikersnaam() {
    return gebruikersnaam;
  }

  public void setGebruikersnaam(final String gebruikersnaam) {
    this.gebruikersnaam = gebruikersnaam;
  }

  public String wachtwoord() {
    return wachtwoord;
  }

  public void setWachtwoord(final String wachtwoord) {
    this.wachtwoord = wachtwoord;
  }

  public void setDialect(final String dialect) {
    this.dialect = dialect;
  }

  public SQLDialect sqlDialect() {
    try {
      return dialect == null ? null : SQLDialect.valueOf(dialect.toUpperCase(Locale.ROOT));
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }
}
