# EML-SQL

Na elke verkiezing in Nederland worden de verkiezingsuitslagen door de Kiesraad gepubliceerd in het EML formaat.
EML (Election Markup Language) is een internationale standard om verkiezingsresultaten te publiceren.
Voor Nederland is de Kiesraad verantwoordelijk voor de Nederlandse variant van deze standard.
Zie [https://www.kiesraad.nl/verkiezingen/osv-en-eml/eml-standaard](https://www.kiesraad.nl/verkiezingen/osv-en-eml/eml-standaard)

Met de applicatie kunnen deze EML bestanden worden ingelezen en in een database worden ingelezen.
Er worden verschillende databases ondersteund, op dit moment zijn dat SQLite en PostgreSQL.
Er zijn ook verschillende database views beschikbaar, waarmee de gegevens eenvoudiger kunnen worden gelezen.
De applicatie is geschreven in Java en maakt gebruik van een gelieerde bibliotheek die de EML bestanden kan inlezen:
[https://gitlab.com/nl-h72/eml-hulpmiddelen](https://gitlab.com/nl-h72//eml-hulpmiddelen)

## Gebruik

Om de applicatie te gebruiken moeten een aantal instellingen gemaakt worden, zoals database verbindingsgegevens.
Ook ondersteunt de applicatie verschillende opties om de gegevensinvoer te filteren.
Bijvoorbeeld door alleen specifieke stembureaus/gemeenten in de database in te laden,
of door geen gegevens op te slaan van personen/partijen met 0 stemmen.

### Configuratie bestand

Een aantal instellingen vaak gebruikte kunnen in een configuratie bestand gezet worden.
Dit configuratie bestand moet in dezelfde directory worden geplaatst waar de applicatie wordt opgestart.
De naam van dit configuratiebestand moet voldoen aan de Spring Boot naam conventie van de [configuratie bestanden](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config.files). Bijvoorbeeld: `application.yaml`.
De volgende instellingen kunnen worden opgegeven:

```yaml
database:
  dialect: [dialect]
  url: [url]
  gebruikersnaam: [gebruikersnaam]
  wachtwoord: [wachtwoord]
parallel: [parallel]
filterNulStemmen: [filterNulStemmen]
```

| Optie                | Uitleg                                                                                                                                                        |
|----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `[dialect]`          | Geef aan welke database gebruikt wordt. Geldige waarden zijn: `"sqlite"` of `"postgres"`                                                                      |
| `[url]`              | Verwijzing naar de database<br>Voor SQLite naar het bestand, b.v. `".../eml.sqlite"`<br>Voor PostgreSQL naar de database url, b.v. `"//localhost/uitslagen"`. |
| `[gebruikersnaam]`   | Gebruikersnaam voor toegang tot de database indien nodig                                                                                                      |
| `[wachtwoord]`       | Wachtwoord voor toegang tot de database indien nodig                                                                                                          |
| `[parallel]`         | Aantal processen dat wordt gebruikt om de gegevens parallel te verwerken<sup>1</sup>. Indien niet opgegeven wordt 1 gebruikt.                                 |
| `[filterNulStemmen]` | Als `true` worden alle partijen en kandidaten met 0 stemmen niet in de database opgeslagen.                                                                   |

<sup>1</sup> Om het inlezen te versnellen kunnen meerdere processen parallel worden opgestart.
Afhankelijk van de snelheid/aantal cores van een computer en van type database kan dit het process versnellen. Dit kan het best proefondervindelijk worden vastgesteld.

Deze instellingen kunnen altijd worden overschreven worden bij het opstarten van de applicatie door op de opdrachtregel andere waarden mee te geven.

### Opdrachtregel opties

Bij het opstarten van de applicatie zijn er verschillende opdrachtregel opties.
Er kan gekozen worden om het bovenstaande configuratiebestand te gebruiken om de opties in stellen,
maar deze kunnen ook via de opdrachtregel worden meegegeven.
Bijvoorbeeld het filter kan als volgt worden meegegeven `--filterNulStemmen=true`.

Het opstarten van het jar bestand met java op de opdrachtregel gaat als volgt:

```sh
java -jar eml-sql-1.0.0.jar [opties] [uitslagen map]
```
(`1.0` is hier als voorbeeld van een versienummer van de jar gebruikt, maar de naam van het jar bestand moet in gebruik de naam van het bestand zijn zoals gebruikt)

## Database Tabellen

| Tabel                            | EML bestand                 | Beschrijving                                                    |
|----------------------------------|-----------------------------|-----------------------------------------------------------------|
| verkiezingen                     | eml110a                     | Algemene gegevens verkiezing                                    |
| kieskringen                      | eml230b                     | Kieskringen                                                     |
| regios                           | eml110a                     | Regios zoals genoemd in het EML110a bestand                     |
| rapportage_gebieden              | eml510                      | Gebied waarin stemmen geteld zijn, van stembureau tot landelijk |
| rapportage_gebieden_statistieken | eml510                      | Statistieken per rapportage gebied met betrekking tot stemmem   |
| partijen                         | eml230b                     | Partijen per kieskring                                          |
| partijen_stemmen                 | eml510                      | Stemmen per partij per rapportage gebied                        |
| kandidaten                       | eml230b,eml510d<sup>2</sup> | Kandidaten per kieskring                                        |
| kandidaten_stemmen               | eml510                      | Stemmen per kandidaat per rapportage gebied                     |
| gekozen_kandidaten               | eml520                      | Gekozen kandidaten<sup>3</sup>                                  |

<sup>2</sup> Voor de 2e kamer verkiezingen worden kandidaten voor de landelijke lijst uit het 510d bestand gehaald,
omdat deze niet in het 230b bestand staan.

<sup>3</sup> Voor 2e kamer verkiezingen is het id van de kandidaat in de database niet hetzelfde id als in het eml520 bestand.
Dit komt omdat in het eml520 een eigen nummering wordt gebruikt en in de database tabel het id dat overeen komt met het id in de kandidaten tabel.

## Views

Views worden gebruikt om eenvoudig queries te groeperen zodat met een eenvoudig aanroep gegevens uit de database kunnen worden gehaald.

- Views zijn nog in ontwikkeling.

## Bouwen van de applicatie

Om de applicatie te compileren is Java 17 (of hoger) en Maven nodig.
De applicatie kan dan gebouwd worden met het standard maven commando `mvn install`.

### Tabelstructuur aanpassen

De applicatie maakt gebruik van JOOQ.
Daarmee kunnen Java classen gegeneert worden om met de database te kunnen communiceren.
Als een aanpasing in de sql tabel structuur gemaakt wordt moeten deze genereerde Java classes opnieuw worden gegeneerd worden.
Met de unit test `DatabaseMakerTest` kan een sqlite database worden aangemaakt die gebruikt wordt door de `pom.xml` om de Java classen te genereren.:

```
mvn -Dtest=DatabaseMakerTest test
```

Met JOOQ generator kunnen nu nieuwe Java classen aangemaakt worden:

```
mvn generate-sources -P database
```

In de `pom.xml` staat de database connectie information.
Eventueel kan door het zetten van deze parameter ook een andere database gebruikt worden om de Java classen te genereren.
Bijvoorbeeld door de tabellen handmatig in de database aan te maken en dan het generate-sources commando te draaien.

## Licentie

Copyright 2024 EML-SQL bijdragers

In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden goedgekeurd door de
Europese  Commissie - latere versies van de EUPL (De "Licentie");

U mag dit werk alleen gebruiken in overeenstemming met de licentie.
U kunt een kopie van de licentie verkrijgen op:

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen, wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op een "AS IS"-basis,
ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK, expliciet of impliciet.
Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen onder de Licentie van toepassing zijn.
